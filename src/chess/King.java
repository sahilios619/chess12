/**
 * 
 */
package chess;

/**
 * Class representing instance of King.
 *  
 * @author dixitadhami
 * @author Sahil Patel
 *
 */
public class King extends Piece {
	
	/**
	 * Color of the piece.
	 */
	public String color;

	/**
	 * 
	 * @param color  	Color of the piece.
	 */
	public King(String color) {
		super(color);
		this.color = color;
	}
	
	/**
	 * @return String specifying the color and piece.
	 */
	public String toString() {
		return color+"K ";
	
	}

	/**
	 * Returns boolean after checking if the move is valid or not.
	 * 
	 * @param input		User's input received.
	 * @param board 	Current game board holding the pieces.
	 * @return Boolean. True if King has valid move, false otherwise.
	 *  
	 */
	@Override
	public boolean isValidMove(String input, Piece[][] board) {
		// TODO Auto-generated method stub
		String[] args = input.split(" ");
		char initFile = args[0].charAt(0);
		char initRank = args[0].charAt(1);
		char finalFile = args[1].charAt(0);
		char finalRank = args[1].charAt(1);

		//Move in free space
		if (Math.abs(initRank - finalRank) <= 1 && Math.abs(initFile - finalFile) <= 1 && board[56 - finalRank][finalFile-'a'] == null){
			return true;
		}
		//Capture piece
		if (Math.abs(initRank - finalRank) <= 1 && Math.abs(initFile - finalFile) <= 1) {
			if((isWhite && !board[56 - finalRank][finalFile-'a'].isWhite) || (!isWhite && board[56 - finalRank][finalFile-'a'].isWhite)){
				return true;
			}
		}
		//Castling
		if (!board[56 - initRank][initFile - 'a'].hasMoved()){
			if(finalFile == 'g' && (56 - finalRank == 0 || 56 - finalRank == 7)
					&& board[56 - finalRank][7] instanceof Rook
					&& !board[56 - finalRank][7].hasMoved()
					&& board[56 - finalRank][7].isWhite() == isWhite()){
				if((board[7][5] == null && board[7][6] == null) || (board[0][5] == null && board[0][6] == null)) 
					return true;	
			}
			if(finalFile == 'c' && (56 - finalRank == 0 || 56 - finalRank == 7)
					&& board[56 - finalRank][0] instanceof Rook
					&& !board[56 - finalRank][0].hasMoved()
					&& board[56 - finalRank][0].isWhite() == isWhite()){
				if((board[7][1] == null && board[7][2] == null && board[7][3] == null) || (board[0][1] == null && board[0][2] == null && board[0][3] == null)) 
					return true;
			}
		}
		return false;
	}

	/** 
	 * Returns an updated board after moving.
	 * 
	 * @param input		User's input received.
	 * @param board		Current game board holding the pieces.
	 * @return The updated game board.
	 * 
	 */
	@Override
	public Piece[][] move(String input, Piece[][] board) {
		// TODO Auto-generated method stub
		
		String[] args = input.split(" ");
		char initFile = args[0].charAt(0);
		char initRank = args[0].charAt(1);
		char finalFile = args[1].charAt(0);
		char finalRank = args[1].charAt(1);
		
		if (!hasMoved() && finalFile == 'g' && (finalRank-48 == 1 || finalRank-48 == 8)
			&& board[56 - finalRank][7] instanceof Rook
			&& !board[56 - finalRank][7].hasMoved()
			&& board[56 - finalRank][7].isWhite() == isWhite()){			
			if((board[7][5] == null && board[7][6] == null) || (board[0][5] == null && board[0][6] == null)) 
				board = castle(initRank, 7, board);
		}
		else if (!hasMoved() && finalFile == 'c' && (finalRank-48 == 1 || finalRank-48 == 8)
			&& board[56 - finalRank][0] instanceof Rook
			&& !board[56 - finalRank][0].hasMoved()
			&& board[56 - finalRank][0].isWhite() == isWhite()) {
			
			if((board[7][1] == null && board[7][2] == null && board[7][3] == null) || (board[0][1] == null && board[0][2] == null && board[0][3] == null)) 
				board = castle(initRank, 0, board);		
		}
		else {
			Piece initPiece = board[56 - initRank][initFile-'a'];
		
			board[56 - finalRank][finalFile-'a'] = initPiece;
			board[56 - initRank][initFile-'a'] = null;
		}
		return board;
	}
	
	/**
	 * Returns an updated board
	 *  
	 * @param initRank		Initial Rank - row of initial piece.
	 * @param rookFile		Rook File - column of destination rook piece.
	 * @param board			Current game board holding the pieces.
	 * @return The updated game board.
	 * 
	 */
	public Piece[][] castle (char initRank, int rookFile, Piece[][] board) {
		Piece king = board[56 - initRank][4];
		Piece rook = board[56 - initRank][rookFile];
		
		if (rookFile == 7) {
			board[56 - initRank][6] = (king);
			board[56 - initRank][5] = (rook);
			board[56 - initRank][7] = (null);
			board[56 - initRank][4] = (null);
		}
		if (rookFile == 0) {
			board[56 - initRank][2] = (king);
			board[56 - initRank][3] = (rook);
			board[56 - initRank][0] = (null);
			board[56 - initRank][1] = (null);
			board[56 - initRank][4] = (null);
		}
		
		return board;
	}
}