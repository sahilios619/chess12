/**
 * 
 */
package chess;

/**
 * Class representing instance of Pawn.
 *  
 * @author dixitadhami
 * @author Sahil Patel
 *
 */
public class Pawn extends Piece {
	
	/**
	 * Color of the piece
	 */
	public String color;
	
	/**
	 * 
	 * @param color  	Color of the piece.
	 */
	public Pawn(String color) {
		super(color);
		this.color = color;
	}
	
	/**
	 * @return String specifying the color and piece.
	 */
	public String toString() {
		return color+"p ";
	}

	/**
	 * Returns boolean after checking if the move is valid or not.
	 * 
	 * @param input		User's input received.
	 * @param board 	Current game board holding the pieces.
	 * @return Boolean. True if Pawn has valid move, false otherwise.
	 *  
	 */
	@Override
	public boolean isValidMove(String input, Piece[][] board) {
		// TODO Auto-generated method stub
		String[] args = input.split(" ");
		char initFile = args[0].charAt(0);
		char initRank = args[0].charAt(1);
		char finalFile = args[1].charAt(0);
		char finalRank = args[1].charAt(1);
				
		if (initFile == finalFile) {
				
			// White pawn moves 1 space
			if (isWhite && initRank+1 == finalRank && board[56 - finalRank][finalFile-'a'] == null){
				return true;
			}
			
			// White pawn moves 2 spaces
			if (isWhite && initRank == '2' && initRank+2 == finalRank && board[56 - finalRank + 1][finalFile-'a'] == null && board[56 - finalRank][finalFile-'a'] == null){
				return true;
			}

			// Black pawn moves 1 space
			if (!isWhite && initRank-1 == finalRank && board[56 - finalRank][finalFile-'a'] == null){
				return true;
			}
			
			// Black pawn moves 2 spaces
			if (!isWhite && initRank == '7' && initRank-2 == finalRank  && board[56 - finalRank - 1][finalFile-'a'] == null && board[56 - finalRank][finalFile-'a'] == null){
				return true;
			}
		}
		
		//En Passant
			try {
				if (board[56 - finalRank][finalFile-'a'] == null && isWhite && initRank+1 == finalRank && initFile-1 == finalFile && !board[56 - initRank][finalFile-'a'].isWhite && board[56 - initRank][finalFile-'a'].enpassantCapture){
						return true;				
				}
				else if (board[56 - finalRank][finalFile-'a'] == null && isWhite && initRank+1 == finalRank && initFile+1 == finalFile && !board[56 - initRank][finalFile-'a'].isWhite && board[56 - initRank][finalFile-'a'].enpassantCapture){
						return true;					
				}
				else if (board[56 - finalRank][finalFile-'a'] == null && !isWhite && initRank-1 == finalRank && initFile-1 == finalFile && board[56 - initRank][finalFile-'a'].isWhite && board[56 - initRank][finalFile-'a'].enpassantCapture){
						return true;				
				}
				else if (board[56 - finalRank][finalFile-'a'] == null && !isWhite && initRank+1 == finalRank && initFile+1 == finalFile && board[56 - initRank][finalFile-'a'].isWhite && board[56 - initRank][finalFile-'a'].enpassantCapture){
						return true;	
				}
			}catch(Exception e) {
			}
			
		//Capture
		if (initFile-1 == finalFile && board[56 - finalRank][finalFile-'a'] != null) {
			if (isWhite && initRank+1 == finalRank && !board[56 - finalRank][finalFile-'a'].isWhite){
				return true;
			}
			if (!isWhite && initRank-1 == finalRank && board[56 - finalRank][finalFile-'a'].isWhite){
				return true;
			}
		}
		else if (initFile+1 == finalFile && board[56 - finalRank][finalFile-'a'] != null) {
			if (isWhite && initRank+1 == finalRank && !board[56 - finalRank][finalFile-'a'].isWhite){
				return true;
			}
			if (!isWhite && initRank-1 == finalRank && board[56 - finalRank][finalFile-'a'].isWhite){
				return true;
			}
		}
		return false;
	}

	/** 
	 * Returns an updated board after moving.
	 * 
	 * @param input		User's input received.
	 * @param board		Current game board holding the pieces.
	 * @return The updated game board.
	 * 
	 */
	@Override
	public Piece[][] move(String input, Piece[][] board) {
		
	    String[] args = input.split(" ");
		char initFile = args[0].charAt(0);
		char initRank = args[0].charAt(1);
		char finalFile = args[1].charAt(0);
		char finalRank = args[1].charAt(1);
		
		Piece initPiece = board[56 - initRank][initFile-'a'];
		
		try {
			if (board[56 - finalRank][finalFile-'a'] == null && isWhite && initRank+1 == finalRank && initFile-1 == finalFile && !board[56 - initRank][finalFile-'a'].isWhite && board[56 - initRank][finalFile-'a'].enpassantCapture ||				
					board[56 - finalRank][finalFile-'a'] == null && !isWhite && initRank-1 == finalRank && initFile-1 == finalFile && board[56 - initRank][finalFile-'a'].isWhite && board[56 - initRank][finalFile-'a'].enpassantCapture){
				board[56 - initRank][initFile-'a'-1] = null;
			}
			else if (board[56 - finalRank][finalFile-'a'] == null && isWhite && initRank+1 == finalRank && initFile+1 == finalFile && !board[56 - initRank][finalFile-'a'].isWhite && board[56 - initRank][finalFile-'a'].enpassantCapture ||
					board[56 - finalRank][finalFile-'a'] == null && !isWhite && initRank+1 == finalRank && initFile+1 == finalFile && board[56 - initRank][finalFile-'a'].isWhite && board[56 - initRank][finalFile-'a'].enpassantCapture){
				board[56 - initRank][initFile-'a'+1] = null;
			}
		}catch(Exception e) {
			
		}
		board[56 - finalRank][finalFile-'a'] = initPiece;
		board[56 - initRank][initFile-'a'] = null;
		
		if (isWhite && finalRank == '8')
			promote(input, finalFile, finalRank, board, "w");
		if (!isWhite && finalRank == '1')
			promote(input, finalFile, finalRank, board, "b");
		
		return board;
	}
	
	/**
	 * Indicates the promotion of the Pawn piece after the move.
	 * 
	 * @param input		User's input received.
	 * @param finalFile Final File - column of the destination.
	 * @param finalRank	Final Rank - row of the destination.
	 * @param board		Current game board holding the pieces.
	 * @param color		Color of the piece.
	 * 
	 */
	public void promote(String input, char finalFile, char finalRank, Piece[][] board, String color) {
		String[] args = input.split(" ");
		if (args.length == 3) {
			switch(args[2])
			{
				case "q":
					board[56 - finalRank][finalFile-'a'] = (new Queen(color));
					return;
				case "n":
					board[56 - finalRank][finalFile-'a'] = (new Knight(color));
					return;
				case "r":
					board[56 - finalRank][finalFile-'a'] = (new Rook(color));
					return;
				case "b":
					board[56 - finalRank][finalFile-'a'] = (new Bishop(color));
					return;
			}
		}
		else
			board[56 - finalRank][finalFile-'a'] = (new Queen(color));
	}
}
