/**
 * 
 */
package chess;

/**
 * Abstract super class for all Chess pieces.
 * 
 * @author dixitadhami
 * @author Sahil Patel
 * 
 */
public abstract class Piece {
	boolean enpassantCapture;
	boolean hasMoved;
	boolean isWhite;
	String type;
	
	/**
	 * 
	 * @param color		Color of the piece.
	 */
	public Piece(String color) {
		isWhite = color.equals("w");
		hasMoved = false;
	}
	
	/**
	 * Returns a boolean if the piece is white or not.
	 * 
	 * @return Boolean.		True if piece is White, false otherwise.	
	 *  
	 */
	public boolean isWhite() {
		return isWhite;
	}
	
	/**
	 * Returns boolean after checking if the piece has moved or not.
	 * 
	 * @return Boolean.		True is the piece is moved, false otherwise.
	 * 
	 */
	public boolean hasMoved() {
		return hasMoved;
	}
	
	/**
	 * Returns boolean after checking if the move is valid or not.
	 * 
	 * @param input		User's input received.
	 * @param board 	Current game board holding the pieces.
	 * @return Boolean. True if Piece has valid move, false otherwise.
	 *  
	 */
	public abstract boolean isValidMove(String input, Piece[][] board);
	
	/** 
	 * Returns an updated board after moving.
	 * 
	 * @param input		User's input received.
	 * @param board		Current game board holding the pieces.
	 * @return The updated game board.
	 * 
	 */
	public abstract Piece[][] move(String input, Piece[][] board);

}
