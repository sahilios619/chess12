/**
 * 
 */
package chess;

/**
 * 
 * Class representing instance of Bishop.
 * 
 * @author Dixita Dhami
 * @author Sahil Patel
 *
 */
public class Bishop extends Piece {

	/**
	 * Color of the piece.
	 */
	public String color;

	/**
	 * 
	 * @param color  	Color of the piece.
	 */
	public Bishop(String color) {
		super(color);
		this.color = color;
	}

	/**
	 * @return String specifying the color and piece.
	 */
	public String toString() {
		return color+"B ";
	}

	/**
	 * Returns boolean after checking if the move is valid or not.
	 * 
	 * @param input		User's input received.
	 * @param board 	Current game board holding the pieces.
	 * @return Boolean.	True if bishop has valid move, false otherwise.
	 */
	@Override
	public boolean isValidMove(String input, Piece[][] board) {
		// TODO Auto-generated method stub
		String[] args = input.split(" ");
		char initFile = args[0].charAt(0);
		char initRank = args[0].charAt(1);
		char finalFile = args[1].charAt(0);
		char finalRank = args[1].charAt(1);
	
		if (Math.abs(initFile - finalFile) == Math.abs(initRank - finalRank)) { 
			if(!hasObstacle(initFile, initRank, finalFile, finalRank, board)) {
				if (board[56-finalRank][finalFile-'a'] == null)
					return true;
				if (board[56-finalRank][finalFile-'a'].isWhite != board[56-initRank][initFile-'a'].isWhite)
					return true;
			}
		}
	return false;
	}
	
	/** 
	 * Returns an updated board after moving.
	 * 
	 * @param input		User's input received.
	 * @param board		Current game board holding the pieces.
	 * @return The updated game board.
	 * 
	 */
	@Override
	public Piece[][] move(String input, Piece[][] board) {
		// TODO Auto-generated method stub
		String[] args = input.split(" ");
		char initFile = args[0].charAt(0);
		char initRank = args[0].charAt(1);
		char finalFile = args[1].charAt(0);
		char finalRank = args[1].charAt(1);
		
		Piece initPiece = board[56 - initRank][initFile-'a'];
	
		board[56 - finalRank][finalFile-'a'] = initPiece;
		board[56 - initRank][initFile-'a'] = null;
		
		return board;
	}
	
	/**
	 * Returns a boolean after checking for an obstacle on the path.
	 * 
	 * @param initFile		Initial File - column of initial piece.
	 * @param initRank		Initial Rank - row of initial piece.
	 * @param finalFile		Final File - column of destination.
	 * @param finalRank		Final Rank - row of destination.
	 * @param board			Current game board holding the pieces.
	 * @return Boolean.		True if obstacle exists, false otherwise.
	 * 
	 */
	public boolean hasObstacle(char initFile, char initRank, char finalFile, char finalRank, Piece[][] board) {
		int iRank = 56-initRank;
		int fRank = 56-finalRank;
		int iFile = initFile-'a';
		int fFile = finalFile-'a';
		int rankDif = iRank - fRank;
		int fileDif = iFile - fFile;
		
		if (Math.abs(rankDif) == Math.abs(fileDif)) {
	
			// up & right 
			if (fileDif < 0 && rankDif > 0) {
				int r = iRank-1;
				for (int f = iFile+1; f < fFile; f++) {
	
					if (board[r][f] != null)
						return true;
					r--;
				}
			}
	
			// up & left  
			if (fileDif > 0 && rankDif > 0) {
				int r = iRank-1;
				for (int f = iFile-1; f > fFile; f--) {
					if (board[r][f] != null)
						return true;
					r--;
				}
			}
	
			// down & right 
			if (fileDif < 0 && rankDif < 0) {
				int r = iRank+1;
				for (int f = iFile+1; f < fFile; f++) {
					if (board[r][f] != null)
						return true;
					r++;
				}
			}
	
			// down & left 
			if (fileDif > 0 && rankDif < 0) {
				int r= iRank+1;
				for (int f = iFile-1; f > fFile; f--) {
					if (board[r][f] != null)
						return true;
					r++;
				}
			}
		}
		return false;
	}
}