/**
 * 
 */
package chess;

/**
 * Class representing The Chess Game Board and Functions.
 * 
 * @author Dixita Dhami
 * @author Sahil Patel
 *
 */
public class Board {
	
	private boolean isDone;
	private boolean isWhitesMove;
	private boolean whiteInCheck;
	private boolean blackInCheck;
	private boolean drawOptionAvailable;
	public static Piece[][] board;
	
	public  Board() {
	}
	
	/**
	 * Initialization of pieces and setup on the board.
	 */
	public void createBoard() {
		
		isDone = false;
		isWhitesMove = true;
		whiteInCheck = false;
		blackInCheck = false;
		drawOptionAvailable = false;
				
		board = new Piece[8][8];
		
		board[0][0] = new Rook("b");
		board[0][7] = new Rook("b");
		board[7][0] = new Rook("w");
		board[7][7] = new Rook("w");
		
		board[0][1] = new Knight("b");
		board[0][6] = new Knight("b");
		board[7][1] = new Knight("w");
		board[7][6] = new Knight("w");
		
		board[0][2] = new Bishop("b");
		board[0][5] = new Bishop("b");
		board[7][2] = new Bishop("w");
		board[7][5] = new Bishop("w");
		
		board[0][3] = new Queen("b");
		board[7][3] = new Queen("w");
		
		board[0][4] = new King("b");
		board[7][4] = new King("w");
		
		board[1][0] = new Pawn("b");
		board[1][1] = new Pawn("b");
		board[1][2] = new Pawn("b");
		board[1][3] = new Pawn("b");
		board[1][4] = new Pawn("b");
		board[1][5] = new Pawn("b");
		board[1][6] = new Pawn("b");
		board[1][7] = new Pawn("b");
		board[6][0] = new Pawn("w");
		board[6][1] = new Pawn("w");
		board[6][2] = new Pawn("w");
		board[6][3] = new Pawn("w");
		board[6][4] = new Pawn("w");
		board[6][5] = new Pawn("w");
		board[6][6] = new Pawn("w");
		board[6][7] = new Pawn("w");	
	}
	
	/**
	 * Output of the Board with setup pieces.
	 */
	public static void showBoard() {
        System.out.println();

		for(int i = 0 ; i<8 ; i++){
	        for(int j = 0 ; j<8 ; j++){
	        	if(board[i][j] == null) {
		            if((i%2 == 0 && j%2 == 0) || (i%2 == 1 && j%2 == 1))
		                System.out.print("   ");
		            else
		                System.out.print("## ");
	        	}
	        	else
	        	{
	        		System.out.print(board[i][j]);
	        	}
	        }
	        System.out.println(8-i);
	    }
	    System.out.println(" a  b  c  d  e  f  g  h \n");
	}
	
	/**
	 * Returns boolean after checking if the move is valid or not.
	 * 
	 * @param input		User's input received.
	 * @return Boolean.	True if bishop has valid move, false otherwise.
	 * 
	 */
	public boolean isValidMove(String input) {
		String[] args = input.split(" ");
		String initPos = args[0];
		String finalPos = args[1];
		char initFile = initPos.charAt(0);
		char initRank = initPos.charAt(1);
		
		// if the position is the same / there's no piece in the selected place / the piece is not the correct player's piece
		if (initPos.equals(finalPos)) {
			return false;
		}
		else if (board[56 - initRank][initFile-'a'] == null) {
			return false;
		}
		else if (board[56 - initRank][initFile-'a'].isWhite() != isWhitesMove()) {
			return false;
		}
		else 
			return board[56 - initRank][initFile-'a'].isValidMove(input, board);
	}
	
	/** 
	 * Returns boolean if piece can move or not.
	 * 
	 * @param input		User's input received.
	 * @return Boolean.	True if piece is allowed to move, false otherwise.
	 * 
	 */
	public boolean move(String input) {
		String[] args = input.split(" ");
		char initFile = args[0].charAt(0);
		char initRank = args[0].charAt(1);
		char finalFile = args[1].charAt(0);
		char finalRank = args[1].charAt(1);
		
		Piece[][] copy = copy(board);
		copy = copy[56 - initRank][initFile-'a'].move(input, copy);	
	
		if (putsSelfInCheck(copy,"")) {
			
			System.out.println("Illegal move, try again");
			return false;
		}
		else {
			board = board[56 - initRank][initFile-'a'].move(input, board);	
			
			if (putsOpponentInCheck()) {
				changePlayer();
				if(isWhitesMove)
					whiteInCheck = true;	
				else
					blackInCheck = true;
			}
			else {
				try{
					for(int i = 0 ; i<8 ; i++){
				        for(int j = 0 ; j<8 ; j++){
				        	if(board[i][j] instanceof Pawn) 
					           board[i][j].enpassantCapture = false;
				        }
				    }
					if (board[56 - finalRank][finalFile-'a'] instanceof Pawn && Math.abs(initRank - finalRank) == 2){
							board[56 - finalRank][finalFile-'a'].enpassantCapture = true;
					}
				}catch (Exception e) {
					System.out.println(e.toString());
				}
				changePlayer();
			}
		}
		return true;
	}

	/**
	 * Returns boolean after checking if the check is valid or not.
	 * 
	 * @param temp		Temporary piece for the location.	
	 * @param test		Test string to hold the value.
	 * @return Boolean.	True if holds in check, false otherwise.
	 * 
	 */
	public boolean putsSelfInCheck(Piece[][] temp, String test) {
		
		boolean inCheck = false;
		String ownKingLocation = "";
		for(int i = 0 ; i<8 ; i++){
			for(int j = 0 ; j<8 ; j++){
				if(temp[i][j] != null && temp[i][j] instanceof King) {
					if((isWhitesMove && temp[i][j].isWhite) || (!isWhitesMove && !temp[i][j].isWhite)) {
						char rank = (char) (56 - i);
						char file = (char) (j + 'a');
						ownKingLocation = file + "" + rank;	
					}
			    }
			}
		}
		for(int i = 0 ; i<8 ; i++){
			for(int j = 0 ; j<8 ; j++){
				if(temp[i][j] != null) {
					if((!isWhitesMove && temp[i][j].isWhite) || (isWhitesMove && !temp[i][j].isWhite)) {
						char rank = (char) (56 - i);
						char file = (char) (j + 'a');
						String opponentPiece = file + "" + rank;
						if (temp[i][j].isValidMove(opponentPiece +" "+ ownKingLocation, temp)) {
							return true;
						}
						else inCheck = false;	
					}
			    }
			}
		}	
		if(test.equals("")) {
			if(isWhitesMove)
				whiteInCheck = false;
			else
				blackInCheck = false;
		}
		return inCheck;
	}
	
	/**
	 * Returns boolean if opponent is in check or not.
	 * @return Boolean.		True if opponent in check, false otherwise
	 * 
	 */
	public boolean putsOpponentInCheck() {
		
		boolean inCheck = false;
		String opponentKingLocation = "";
		for(int i = 0 ; i<8 ; i++){
			for(int j = 0 ; j<8 ; j++){
				if(board[i][j] != null && board[i][j] instanceof King) {
					if((isWhitesMove && !board[i][j].isWhite) || (!isWhitesMove && board[i][j].isWhite)) {
						char rank = (char) (56 - i);
						char file = (char) (j + 'a');
						opponentKingLocation = file + "" + rank;	
					}
			    }
			}
		}
		for(int i = 0 ; i<8 ; i++){
			for(int j = 0 ; j<8 ; j++){
				if(board[i][j] != null) {
					if((isWhitesMove && board[i][j].isWhite) || (!isWhitesMove && !board[i][j].isWhite)) {
						char rank = (char) (56 - i);
						char file = (char) (j + 'a');
						String ownPiece = file + "" + rank;
						if (board[i][j].isValidMove(ownPiece +" "+ opponentKingLocation, board)) {
							return true;
						}
						else inCheck = false;	
					}
			    }
			}
		}		
		return inCheck;
	}
	
	/**
	 * Returns boolean for checkmate.
	 * @return Boolean.		True if checkmate, false otherwise
	 * 
	 */
	public boolean checkmate() {
		 int validmoves = 0;
		for(int i = 0 ; i<8 ; i++){
			for(int j = 0 ; j<8 ; j++){
				char rank = (char) (56 - i);
				char file = (char) (j + 'a');
				String ownPiece = file + "" + rank;

				if(board[i][j] != null ) {
					if((isWhitesMove && board[i][j].isWhite) || (!isWhitesMove && !board[i][j].isWhite)) {
						
						for(int r = 0 ; r<8 ; r++){
							for(int f = 0 ; f<8 ; f++){

								char altrank = (char) (56 - r);
								char altfile = (char) (f + 'a');
								String otherMove = altfile + "" + altrank;
								String input = ownPiece +" "+ otherMove;
								Piece[][] copy = copy(board);
								
								if(copy[i][j].isValidMove(input, copy)) {
										
									copy = copy[i][j].move(input, copy);

									if (!putsSelfInCheck(copy, "test")) {
										validmoves++;
									}
								}
								
							}
						}
						
					}
			    }
			}
		}
		return (validmoves==0);
	}
	
	/**
	 * Returns the copied Piece[][].
	 * @param board		User's input received.
	 * @return Returns the temporary Piece[][].
	 */
	public Piece[][] copy (Piece[][] board){
		Piece[][] temp = new Piece[8][8];
		for(int i = 0 ; i<8 ; i++){
	        for(int j = 0 ; j<8 ; j++){
	        	temp[i][j] = board[i][j]; 
	        }
	    }
		return temp;
	}
	
	/**
	 *  
	 * @return Boolean.		True if opponent asks for a draw, false otherwise.
	 */
	public boolean isDrawOptionAvailable() {
		return drawOptionAvailable;
	}
	
	/**
	 * 
	 * Enables the draw option for user to ask for a draw.
	 */
	public void enableDrawOption() {
		drawOptionAvailable = true;
	}
	
	/**
	 * 
	 * Disables the draw option so user can't ask for a draw.
	 */
	public void disableDrawOption() {
		drawOptionAvailable = false;
	}
	
	/**
	 * 
	 * @return Boolean.		True if it is White's move, false otherwise.
	 */
	public boolean isWhitesMove() {
		return isWhitesMove;
	}
	
	/**
	 * 
	 * Player is changed, cannot move white pieces.
	 */
	public void changePlayer() {
		isWhitesMove = !isWhitesMove;
	}
	
	/**
	 * 
	 * @return Boolean.		True if the game is done, false otherwise.
	 */
	public boolean isDone() {
		return isDone;
	}
	
	/**
	 * 
	 * @return Boolean.		True if white is in check, false otherwise.
	 */
	public boolean whiteInCheck() {
		return whiteInCheck;
	}
	
	/**
	 * 
	 * @return Boolean.		True if black is in check, false otherwise.
	 */
	public boolean blackInCheck() {
		return blackInCheck;
	}
}

