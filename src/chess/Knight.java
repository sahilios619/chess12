/**
 * 
 */
package chess;

/**
 * Class representing instance of Knight.
 *  
 * @author dixitadhami
 * @author Sahil Patel
 *
 */
public class Knight extends Piece {
	
	/**
	 * Color of the piece
	 */
	public String color;

	/**
	 * 
	 * @param color  	Color of the piece.
	 */
	public Knight(String color) {
		super(color);
		this.color = color;
	}
	
	/**
	 * @return String specifying the color and piece.
	 */
	public String toString() {
		return color+"N ";
	}

	/**
	 * Returns boolean after checking if the move is valid or not.
	 * 
	 * @param input		User's input received.
	 * @param board 	Current game board holding the pieces.
	 * @return Boolean. True if Knight has valid move, false otherwise.
	 *  
	 */
	@Override
	public boolean isValidMove(String input, Piece[][] board) {
		// TODO Auto-generated method stub
		String[] args = input.split(" ");
		char initFile = args[0].charAt(0);
		char initRank = args[0].charAt(1);
		char finalFile = args[1].charAt(0);
		char finalRank = args[1].charAt(1);
				
		//Move knight to empty space		
		if (Math.abs(initFile - finalFile) == 1 && Math.abs(initRank - finalRank) == 2 && board[56 - finalRank][finalFile-'a'] == null)
			return true;
		
		else if (Math.abs(initFile - finalFile) == 2 && Math.abs(initRank - finalRank) == 1 && board[56 - finalRank][finalFile-'a'] == null)
			return true;
		
		//Move knight to capture piece
		else if (Math.abs(initFile - finalFile) == 1 && Math.abs(initRank - finalRank) == 2 && 
				board[56 - finalRank][finalFile-'a'].isWhite != board[56 - initRank][initFile-'a'].isWhite) 
			return true;
		
		else if (Math.abs(initFile - finalFile) == 2 && Math.abs(initRank - finalRank) == 1 && 
				board[56 - finalRank][finalFile-'a'].isWhite != board[56 - initRank][initFile-'a'].isWhite) 
			return true;
		
		return false;
		
	}

	/** 
	 * Returns an updated board after moving.
	 * 
	 * @param input		User's input received.
	 * @param board		Current game board holding the pieces.
	 * @return The updated game board.
	 * 
	 */
	@Override
	public Piece[][] move(String input, Piece[][] board) {
		// TODO Auto-generated method stub
		String[] args = input.split(" ");
		char initFile = args[0].charAt(0);
		char initRank = args[0].charAt(1);
		char finalFile = args[1].charAt(0);
		char finalRank = args[1].charAt(1);
		
		Piece initPiece = board[56 - initRank][initFile-'a'];
		board[56 - finalRank][finalFile-'a'] = initPiece;
		board[56 - initRank][initFile-'a'] = null;
				
		return board;
	}
}
