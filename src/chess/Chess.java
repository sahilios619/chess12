/**
 *
 */
package chess;
import java.util.Scanner;

/**
 * Main Chess file to run the Game
 * 
 * @author dixitadhami
 * @author Sahil Patel
 *
 */
public class Chess {
private static Board game;
static boolean gameInProgress = true;
private static Scanner scan = new Scanner(System.in);

	/**
	 * Main method - calls start method
	 * @param args	arguments
	 */
	public static void main(String[] args) {
		start();
	}
	
	/**
	 * 
	 * Start of the game
	 */
	public static void start() {
		String input;
		boolean validInput, validMove;
		game = new Board();
		game.createBoard();
		game.showBoard();
		while (!game.isDone()) {
			
			if (game.isWhitesMove()) {
				if(game.whiteInCheck())
					if (game.checkmate()) {
						System.out.println("Checkmate");
						System.out.println("Black wins");
						System.exit(0);
					}
					else
						System.out.println("Check");
				if(!game.whiteInCheck())
					if (game.checkmate()) {
						System.out.println("Stalemate");
						System.out.println("Draw");
						System.exit(0);
					}
				System.out.print("White's move: ");
			}
			else {
				if(game.blackInCheck())
					if (game.checkmate()) {
						System.out.println("Checkmate");
						System.out.println("White wins");
						System.exit(0);
					}else
						System.out.println("Check");
				if(!game.blackInCheck())
					if (game.checkmate()) {
						System.out.println("Stalemate");
						System.out.println("Draw");
						System.exit(0);
					}
				System.out.print("Black's move: ");	
			}
			input = scan.nextLine().toLowerCase();
			validInput = isValidInput(input);
			validMove = false;
			
			if (validInput) {
				validMove = game.isValidMove(input);
			}
			else {
				while(!validInput || !validMove) {
					if (!validInput)
						System.out.println("Illegal move, try again");
					else if (!validMove)
						System.out.println("Illegal move, try again");
					
					if (game.isWhitesMove())
						System.out.print("White's move: ");
					else
						System.out.print("Black's move: ");				
					input = scan.nextLine().toLowerCase();
					validInput = isValidInput(input);
					validMove = true;
					if (validInput)
						validMove = game.isValidMove(input);
				}
			}
			if(validMove) {	
				if(game.move(input)) {
				game.showBoard();
				}
			}
			else {
				//game.showBoard();
				System.out.println("Illegal move, try again");
			}
		}
	}
	
	/**
	 * Returns boolean if user input is valid or not.
	 * 
	 * @param input		User's input received.
	 * @return Boolean.	True if input is valid, false otherwise.
	 * 
	 */
	public static boolean isValidInput(String input) {
		//Checks to see if player is resigning the game
		if (input.equalsIgnoreCase("resign"))
		{
			if(game.isWhitesMove()){
				System.out.println("Black wins");
			}
			else{
				System.out.println("White wins");
			}
			System.exit(0);
		}
		
		//Checks to see if player asked for draw
		String[] args = input.split(" ");
		if (args.length == 3)
			if (args[2].equalsIgnoreCase("draw?"))
				game.enableDrawOption();
		
			
		//If the player accepts the draw
		else if (input.equalsIgnoreCase("draw")){
			if(game.isDrawOptionAvailable()) {
				System.out.println("Draw");
				System.exit(0);
			}
		}
		
		//if draw is asked for and other player does not accept draw
		else if(game.isDrawOptionAvailable())
		{
			if (!input.equalsIgnoreCase("draw"))
				game.disableDrawOption();
		}
		
		//Checks if input matches format
		return input.matches("[abcdefgh][12345678] [abcdefgh][12345678]") 
				|| input.matches("[abcdefgh][12345678] [abcdefgh][12345678] [qnrb]")
				|| input.matches("[abcdefgh][12345678] [abcdefgh][12345678] draw[?]");
	
	}
}

