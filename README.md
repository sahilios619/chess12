//Group-12
//Dixita Dhami
//Sahil Patel


2 player chess game for terminal

User enters their move in the form of "FileRank FileRank"

To ask for a draw, append "draw" at the end of your move.
If the other player accepts, the game will end in a draw, otherwise continue playing.

You can quit the game by typing in "resign"

Complete implementation provided including:
	Castling,
	En Passant,
	Pawn Promotion,
	Checkmate,
	Stalemate.